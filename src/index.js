/**
 * Determine the environment and run the appropriate http server
 * @param {Function} app Callback for http.createServer()
 * @param {Number} port The port number to start on
 */
function main(app, port = process.env.PORT) {
	const isProduction = process.env.NODE_ENV === 'production';
	if(isProduction) {
		return main.production(app, port);
	} else {
		return main.development(app, port);
	}
}

main.production = (...args) => require('./prod.js')(...args);
main.development = (...args) => require('./dev.js')(...args);

module.exports = main;

if(require.main === module) {
	const app = require(process.argv[2]);
	main(app, process.argv[3]);
}
