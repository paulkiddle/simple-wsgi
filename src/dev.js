const http = require('http');

/**
 * Creates and stards the development server
 * @param {Function} app A callback for http.createServer
 * @param {Number} port The port number to start on
 */
function main(app, port = 8080) {
	const server = http.createServer(app)	;

	return new Promise(resolve =>
		server.listen(port, ()=> {
			console.log('Listening on http://localhost:'+port)
			resolve(server);
		})
	);
}

module.exports = main;
