const cluster = require('cluster');
const http = require('http');
const os = require('os');

/**
 * Create a cluster and run the web app on each of the workers
 * Based on https://nodejs.org/api/cluster.html
 * @param {Function} app
 * @param {Number} port The port number to start on
 */
function main(app, port = 80) {
	if (cluster.isMaster) {
		console.log(`Master ${process.pid} is running`);

		const numCPUs = os.cpus().length;

		// Fork workers.
		for (let i = 0; i < numCPUs; i++) {
			cluster.fork();
		}

		cluster.on('exit', (worker, code, signal) => {
			console.log(`worker ${worker.process.pid} died`);
		});
	} else {
		// Workers can share any TCP connection
		// In this case it is an HTTP server
		http.createServer(app).listen(port);

		console.log(`Worker ${process.pid} started`);
	}
}

module.exports = main;
