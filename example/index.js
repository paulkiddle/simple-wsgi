// Import the wsgi server
const startServer = require('simple-wsgi');

// Import the wsgi application
const app = require('./application.js');

// We automatically detect wherther the environment is dev or production
// by checking the NODE_ENV env variable
startServer(app);

/*
// Alternatively we can be explicit:
startServer.production(app);
startServer.development(app);
*/
