function main(req, res){
	res.end(
		`<!doctype HTML><html><body>
		This is a really simple example!
		But it is a really simple concept -
		you just export a function with the same signature asthe <a href="https://nodejs.org/api/http.html#http_event_request">http.createServer callback</a>.`
	);
}

module.exports = main;
