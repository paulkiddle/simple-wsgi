const lib = require('../src/index.js');

test('Example test', ()=>{
	expect(lib).toBeTruthy()
})

test('Switches on NODE_ENV', () => {
	lib.development = jest.fn();

	lib(1,2);

	expect(lib.development).toHaveBeenCalledWith(1,2);

	process.env.NODE_ENV = 'production';

	lib.production = jest.fn();

	lib(1,2);

	expect(lib.production).toHaveBeenCalledWith(1,2);
});
