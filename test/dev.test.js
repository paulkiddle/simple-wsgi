const { TestScheduler } = require("jest");

const http = require('http');
const dev = require('../src/dev');

const listen = jest.fn();
const createServer = jest.fn(()=> ({listen}));
http.createServer = createServer;

test('Dev server', () => {
	const app = jest.fn();

	dev(app);

	expect(createServer).toHaveBeenCalledWith(app);
	expect(listen).toHaveBeenCalledWith(8080, expect.any(Function));
});
